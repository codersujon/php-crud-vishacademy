<?php require "config/config.php"; ?>

<?php
	// Insert Data  
	if (isset($_POST['submit'])) {
		$fullname = strip_tags($_POST['fullname']);
		$username = strip_tags($_POST['username']);
		$email = strip_tags($_POST['email']);
		$password = md5($_POST['password']);


		if(preg_match('/[^a-zA-Z\s]/',$fullname)){
			echo "Only Alphabets are allowed as fullname";
		}else{
			echo "Full Name looks good!!!";
		}

		if(filter_var($email,FILTER_VALIDATE_EMAIL)){
			echo "Email looks good";
		}else{
			echo "Not a good email address";
		}

		if (strlen($fullname)>2 && strlen($fullname)< 30) {
			echo "Full name is with good length";
		}else{
			echo "Fullname must be between 2 to 30 characters long";
		}

		/*$query = "INSERT INTO `users`(`fullname`, `username`, `email`, `password`) VALUES('$fullname','$username','$email','$password')";
		$fire = mysqli_query($con,$query) or die("Can not insert data into database".mysqli_error($con));
		if($fire) echo "Data Submitted to Database";*/
	}
?>
<?php 
	//Delete Data

	if(isset($_GET['del'])){
		$id = $_GET['del'];
		$query = "DELETE FROM `users` WHERE id= $id";
		$fire = mysqli_query($con,$query) or die("Can not delete the data from the database".mysqli_error($con));
		if($fire) echo "Data Deleted from database";
	}
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>CRUD-2020</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>

	<!-- start form -->
	<div class="container">
		<div class="row">
			<!-- Show Data -->
			<div class="col-lg-8">
				<h3 class="display-4">User Data</h3>
				<hr>

				<table class="table table-striped">
					<thead>
						<tr>
							<th>Full Name</th>
							<th>Username</th>
							<th>Email</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php 

							$query = "SELECT * FROM `users`";

							$fire = mysqli_query($con,$query) or die("Can not fetch data from database.".mysqli_errno($con));

							// if($fire) echo "We got the data from databae.";
							
							 if(mysqli_num_rows($fire)>0){
							 	
							 	while($user = mysqli_fetch_assoc($fire)){ 
							 		?>
							 		<tr>
							 			<td><?= $user['fullname']?></td>
							 			<td><?php echo $user['username']?></td>
							 			<td><?= $user['email']?></td>
							 			<td>
							 				<a href="<?php $_SERVER['PHP_SELF']?>?del=<?php echo $user['id']?>" 
							 					class="btn btn-sm btn-danger">Delete</a>
							 			</td>
							 			<td>
							 				<a href="update.php?upd=<?= $user['id']?>" class="btn btn-sm btn-success">Update</a>
							 			</td>
							 		</tr>
							 		<?php
							 	}
							 }
						 ?>
						
					</tbody>
				</table>
			
			</div>
			<!-- Insert Data -->
			<div class="col-lg-4">
				<h3 class="display-4">Signup</h3>
				<hr>
				<form  name="signup" id="signup" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
					<div class="form-group">
						<label for="fullname">Fullname</label>
						<input type="text" name="fullname" id="fullname" class="form-control" placeholder="Full Name" required>
					</div>
					<div class="form-group">
						<label for="email">Email</label>
						<input type="text" name="email" id="email" class="form-control" placeholder="Email">
					</div>
					<div class="form-group">
						<label for="username">Username</label>
						<input type="text" name="username" id="username" class="form-control" placeholder="Username" required>
					</div>
					<div class="form-group">
						<label for="password">Password</label>
						<input type="password" name="password" id="password" class="form-control" placeholder="Password"required>
					</div>
					<div class="form-group">
						<button class="btn btn-primary btn-block" name="submit" id="submit">Sign Up</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- end form -->




	<!-- JS Includes -->
	<script src="js/bootstrap.min.js"></script>
</body>
</html>