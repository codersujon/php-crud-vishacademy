<?php require "config/config.php"; ?>

<?php  
	if(isset($_GET['upd'])){
		$id = $_GET['upd'];

		$query  = "SELECT * FROM `users` WHERE id = '$id'";
		$fire = mysqli_query($con,$query) or die("Can not fetch the data from database".mysqli_errno($con));

		$user = mysqli_fetch_assoc($fire);
	}
?>
<?php 
	// Update data
	if(isset($_POST['update'])){
		$fullname = $_POST['fullname'];
		$username = $_POST['username'];
		$email = $_POST['email'];
		$password = md5($_POST['password']);

		$query = "UPDATE `users` SET `fullname`='$fullname',`username`='$username',`email`='$email',`password`='$password' WHERE id = '$id'";

		$fire = mysqli_query($con,$query) or die ("Can not update the data.").mysqli_error($con);
		if($fire) header("Location:index.php");
	}

 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>CRUD-2020</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>

	<!-- start form -->
	<div class="container">
		<div class="row">
				<!-- Insert Data -->
				<div class="col-lg-4 col-lg-offset-4">
					<h3 class="display-4">Update Data</h3>
					<hr>
					<form  name="signup" id="signup" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
						<div class="form-group">
							<label for="fullname">Fullname</label>
							<input value="<?= $user['fullname']?>" type="text" name="fullname" id="fullname" class="form-control" placeholder="Full Name">
						</div>
						<div class="form-group">
							<label for="email">Email</label>
							<input value="<?= $user['email']?>" type="email" name="email" id="email" class="form-control" placeholder="Email">
						</div>
						<div class="form-group">
							<label for="username">Username</label>
							<input value="<?= $user["username"]?>" type="text" name="username" id="username" class="form-control" placeholder="Username">
						</div>
						<div class="form-group">
							<label for="password">New Password</label>
							<input value="<?= $user['password']?>" type="password" name="password" id="password" class="form-control" placeholder="Enter New Password">
						</div>
						<div class="form-group">
							<button class="btn btn-primary btn-block" name="update" id="update">Update</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- end form -->

		<!-- JS Includes -->
		<script src="js/bootstrap.min.js"></script>
	</body>
	</html>